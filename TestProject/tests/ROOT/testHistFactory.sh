#!/bin/bash

cd `dirname "$0"`

    
# run the tests and suppress output
python testHistFactory.py > /dev/null

# exit with status code 1 on failure of compare.py
if [[ $? == 1 ]]; then
    exit 1
fi




