#include "TFile.h"

#include "RooMinimizer.h"
#include "RooStats/RooStatsUtils.h"
#include "RooStats/ModelConfig.h"
#include "Math/MinimizerOptions.h"
#include <iostream>

using namespace std;
using namespace RooFit;
using namespace RooStats;

int test_impacts_RooFit()
{
  // Configuration of minimizer
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2", "Migrad");
  ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);

  TFile* file = new TFile("../../test_workspace.root");
  RooWorkspace* ws = (RooWorkspace*)(file->Get("Test"));
  ModelConfig* mc = (ModelConfig*)(ws->obj("ModelConfig"));
  RooAbsPdf* pdf = (RooAbsPdf*)mc->GetPdf();
  RooAbsData* data = (RooAbsData*)(ws->data("asimovData"));
  RooArgSet* nuis = (RooArgSet*)mc->GetNuisanceParameters();
  RooArgSet* globs = (RooArgSet*)mc->GetGlobalObservables();
  RooRealVar* poi = (RooRealVar*)ws->var("mu");


  poi->removeRange();
  poi->setError(0.15);
  poi->setConstant(0);

  RooAbsReal* nll = pdf->createNLL(*data, 
                                  Constrain(*nuis), 
                                  GlobalObservables(*globs), 
                                  Offset(1));
  RooMinimizer m(*nll);
  m.setPrintLevel(-1);

  m.minimize("Minuit2");

  double poi_hat = poi->getVal();

  // save snapshot
  ws->saveSnapshot("tmp_snapshot", *mc->GetPdf()->getParameters(data));

  // results vector
  vector<vector<double>> results(nuis->getSize(), vector<double>(2));

  std::ofstream results_file;
  results_file.open("results.csv");

  int i = 0;
  for (RooLinkedListIter it = nuis->iterator(); RooRealVar* nuip = dynamic_cast<RooRealVar*>(it.Next());) {
    
    cout << nuip->GetName() << endl;
    nuip->setConstant(0);

    double nuip_hat = nuip->getVal();
    double nuip_errup = nuip->getErrorHi();
    double nuip_errdown = nuip->getErrorLo();

    // fix theta at the MLE value +/- postfit uncertainty and minimize again to estimate the change in the POI
    ws->loadSnapshot("tmp_snapshot");
    nuip->setVal(nuip_hat + fabs(nuip_errup));
    nuip->setConstant(1);

    RooMinimizer m_up(*nll);
    m_up.setPrintLevel(-1);

    m_up.minimize("Minuit2");
    double poi_up = poi->getVal();

    ws->loadSnapshot("tmp_snapshot");
    nuip->setVal(nuip_hat - fabs(nuip_errdown));
    nuip->setConstant(1);

    RooMinimizer m_down(*nll);
    m_down.setPrintLevel(-1);

    m_down.minimize("Minuit2");
    double poi_down = poi->getVal();

    cout << "POI when varying NP up by 1 sigma postfit: " << poi_up << endl;
    cout << "POI when varying NP down by 1 sigma postfit: " << poi_down  << endl;

    results_file << nuip->GetName() << "," << poi_down << "," << poi_up << "\n";
  }

  delete file;
  return 0;
}
